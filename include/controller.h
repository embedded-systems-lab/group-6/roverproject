/**
 * @file
 * 
 * Controller Library
 * 
 * Functions to control rover
 * 
 *
 */

#ifndef controller_h
#define controller_h

typedef enum{
  M_DIR_FORWARD = 0, /*Rover direction forward*/
  M_DIR_BACKWARD = 1,/*Rover direction backward*/
  M_DIR_RIGHT = 2,   /*Rover direction towards right*/
  M_DIR_LEFT = 3,    /*Rover direction towards left*/
  M_DIR_LAST         /*Rover stop*/
}M_DIR;  /*Enum for different directions of rover*/

class controller {
  public:
    controller();                                                             /*constructor for controller class*/
    void writemotor(M_DIR state);                                             /*Moves the rover left, right, forward or backward*/
    float gettheta(float targetX, float targetY, float roverX, float roverY); /*Calculates the slope between rover and target in all possible orientations of rover and target 
                                                                                in 4 quadrants divided by x axis and y axis. Rover should rotate by calculated angle to turn
                                                                                towards the target.*/
	  boolean detectBorder(float rover_direction,float roverX, float roverY);   /*Detects if rover is approaching any border*/
};

extern controller controllerobject;

#endif