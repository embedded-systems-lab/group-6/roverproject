/**
 * @file
 * 
 * ESP32 AWS Library
 * 
 * Functions to get the crawler coordinates from the Camera over AWS IoT
 * 
 * Authors: Vipul Deshpande, Jaime Burbano
 */

#ifndef aws_h
#define aws_h

#include <MQTTClient.h>
#include <string>
using namespace std;

void messageHandler(String, String);             /* Function to handle the message coming from AWS topic and extract the co-ordinates */
class myawsclass {
  public:
    myawsclass();                                 /*Constructor for myawsclass class*/
    bool getStatus();                             /*Getter Function to get the status of AWS connection*/
    void connectAWS();                            /*Function to connect to AWS*/
    void publishMessage(int16_t sensorValue);     /*Function to publish information from the rover to AWS*/
    void stayConnected();                         /*Function to run the AWS connection in infinite loop*/
    float getRoverX();                            /*Getter Function to get the x co-ordinate of rover position*/
    float getRoverY();                            /*Getter Function to get the y co-ordinate of rover position*/
    float getRoverDir();                          /*Getter Function to get the direction of rover position*/
    float getTargetX();                           /*Getter Function to get the x co-ordinate of target position*/
    float getTargetY();                           /*Getter Function to get the y co-ordinate of target position*/

};

extern myawsclass awsobject;

#endif

