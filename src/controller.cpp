#include <Arduino.h>
#include "motorDriver.h"
#include "sensorDriver.h"
#include <AWS.h>
#include <cmath>
#include <math.h>
#include "controller.h"

#define MOTOR_SPEED 90       /*Speed of Rover*/
#define BORDER_TOLERANCE 43  /*Distance to be maintained away from all borders*/
#define BORDER_MAX 477       /*Maximum x or y coordinate of the borders*/

/*Constructor for controller class*/
controller::controller() {
}

/** Moves the rover left, right, forward or backward
   @param state : requested rover direction
   @return void 
**/
void controller::writemotor(M_DIR state)
{
  static M_DIR previous_state = M_DIR_LAST;
  static int speed = MOTOR_SPEED;
  if(previous_state == state){
    return;
  }
  switch (state)
  {
    case M_DIR_FORWARD : /*move rover forward*/
      motorobject.set_speed(MotorA,Forward, (1.6 * MOTOR_SPEED));
      motorobject.set_speed(MotorB,Backward, (1.6 * MOTOR_SPEED));
      break;

      case M_DIR_RIGHT :  /* rotate rover towards right*/
      motorobject.set_speed(MotorA,Forward, speed);
      motorobject.set_speed(MotorB,Forward, speed);
      break;

      case M_DIR_LEFT :  /* rotate rover towards left*/
      motorobject.set_speed(MotorA,Backward, speed);
      motorobject.set_speed(MotorB,Backward, speed);
      break;

      case M_DIR_BACKWARD : /*move rover backward*/
      motorobject.set_speed(MotorA,Backward, speed);
      motorobject.set_speed(MotorB,Forward, speed);

    default:
      /* we decide to stop */
      motorobject.set_speed(MotorA,Forward, 0);
      motorobject.set_speed(MotorB,Backward, 0);
      break;
  }
   previous_state = state;
}

/** Calculates the slope between rover and target in all possible orientations of rover and target 
 *  in 4 quadrants divided by x axis and y axis. Rover should rotate by calculated angle to turn
 *  towards the target.
   @param tx1 : target x coordinate
   @param ty1 : target y coordinate
   @param rx1 : rover x coordinate
   @param ry1 : rover y coordinate
   @return angle : slope b/w rover and target
**/
float controller::gettheta(float tx1, float ty1, float rx1, float ry1)
{
  float angle = 0.0;
   
  if (0 == (tx1-rx1)) /*x coordinates of rover and target same*/
  {
    if(ty1>ry1) angle = 270;
    else angle = 90;

    return angle;    
  }
  if (0 == (ty1-ry1)) /*y coordinates of rover and target same*/
  {
    if(tx1>rx1) angle = 359;
    else angle = 180;   
     return angle; 
  }
  float temp = (atan2((ty1-ry1), (tx1-rx1))*180/PI); /*Slope calculated in radians and converted to degrees*/
  if(temp > 0 && temp <= 180) 
  {
    angle = 360-temp;
  }
  else if(temp >= -180 && temp <=0)
  {
    angle = abs(temp);
  }   
  else
  {
    /* do nothing */
  }
   
   return angle;  
}

/** Detects if rover is approaching any border
   @param rover_direction : curent rover direction
   @param rx1 : x coordinate of rover
   @param ry1 : y coordinate of rover
   @return detected : true if border detected otherwise false
**/
boolean controller::detectBorder(float rover_direction,float rx1, float ry1)
{
 boolean detected = false;
 if((rover_direction >= 45)&&(rover_direction <= 135)) /*border check if rover direction is upwards ( b/w 45 to 135 degrees)*/
 {
   if(ry1 <= BORDER_TOLERANCE) detected = true; 
 }
 if((rover_direction > 135)&&(rover_direction <= 225)) /*border check if rover direction is towards left (b/w 135 to 225 degrees)*/
 {
   if(rx1 <= BORDER_TOLERANCE) detected = true; 
 }
 if((rover_direction > 225)&&(rover_direction <= 315)) /*border check if rover direction is downwards (b/w 225 to 315 degrees)*/
 {
   if(abs(ry1-BORDER_MAX) <= BORDER_TOLERANCE) detected = true; 
 }
 if(((rover_direction > 315)&&(rover_direction <=360))||((rover_direction >= 0)&&(rover_direction < 45))) /*border check if rover direction is towards right (b/w 45 to 315 degrees)*/
 {
   if(abs(rx1-BORDER_MAX) <= BORDER_TOLERANCE) detected = true; 
 }
 return detected;
}

controller controllerobject = controller(); /* creating an object of class controller */