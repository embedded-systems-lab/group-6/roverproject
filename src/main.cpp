#include <Arduino.h>
#include "motorDriver.h"
#include "sensorDriver.h"
#include <AWS.h>
#include <cmath>
#include <math.h>
#include "controller.h"
#include "mutex"

/*Task declarations*/
void taskProcessor( void * parameter);
void taskcoordinates( void * parameter);
void taskSensors( void * parameter);

/*#definations*/
#define LED_BOARD 2                   //change here the pin of the board to V2
#define ANGLE_TOL 20                  // Delta Angle tolerance
#define ANGLE_TOL_AT_BORDER 180       // Delta Border tolerance
#define OBJ_DET_DIST 90               // obstacle detection range
#define OBJ_DET_DIST_SEC 36           // obstacle detection range for left and right side
#define TARGET_DELTA  10              //delta range in the target

/*Enum for the task Priorities*/
typedef enum{
  TASK_PRIORITY_WEBCAM = 1,
  TASK_PRIORITY_SENSOR = 2,
  TASK_PRIORITY_PROCESSOR = 3,
  TASK_PRIORITY_LAST
}TASK_PRIORITY;

/*Enum for the Process States*/
typedef enum{
  PROCESS_INIT_STATE = 0,
  PROCESS_OBST_DET_STATE = 1,
  PROCESS_OBST_DET_PROC_STAT = 2,
  PROCESS_CHECK_SLOPE_STATE =3,
  PROCESS_CHECK_SLOPE_PROCESS_STATE = 4,
  PROCESS_CHECK_BORDER = 5,
  PROCESS_LAST
} PROCESS_STATE;

/*Global variables Declarations*/
int SensorData[3];
float rx1, ry1, rdirection1, tx1, ty1, tdirection;// variables to get rover and target coordinates
std::mutex sensorlock, targetlock;

/**
 * @name  void setup() : Function to define and initialize the task and the drivers
   @param None
   @return None 
**/
void setup(){
  pinMode(LED_BOARD, OUTPUT);
  Serial.begin(9600);
  /*Connect to AWS, subsribe*/
  awsobject.connectAWS();
  /*Initialize the sensor drivers*/
  motorobject.SETUP();
  sensorobject.SETUP();
  delay(1000); 
  /*Initialize the tasks*/              
  xTaskCreate(
                    taskcoordinates,  /* Task function. */
                    "taskcoordinates",/* String with name of task. */
                    4096,              /* Stack size in bytes. */
                    NULL,             /* Parameter passed as input of the task */
                    TASK_PRIORITY_WEBCAM,                /* Priority of the task. */
                    NULL);            /* Task handle. */

  xTaskCreate(
                    taskSensors,          /* Task function. */
                    "taskSensor",        /* String with name of task. */
                    4096,              /* Stack size in bytes. */
                    NULL,             /* Parameter passed as input of the task */
                    TASK_PRIORITY_SENSOR,/* Priority of the task. */
                    NULL);            /* Task handle. */
                    
  xTaskCreate(
                    taskProcessor,          /* Task function. */
                    "taskProcessor",        /* String with name of task. */
                    4096,              /* Stack size in bytes. */
                    NULL,             /* Parameter passed as input of the task */
                    TASK_PRIORITY_PROCESSOR,/* Priority of the task. */
                    NULL);               
}

void loop(){
delay(1000);
}

/**
 * @name  void taskcoordinates( void * parameter ) : Function which runs infinitely, task is responsible for be connected to
 * AWS and subsribe the cordinates and calculate the angle.
   @param void * parameter  task parameters can be passed
   @return None 
**/
void taskcoordinates( void * parameter )
{
    for( ;; )
    {
      awsobject.stayConnected();                  // calling to keep connected to aws and subsribe the cordinates
      /*refresh the cordinates of rover and target*/
	    rx1 = awsobject.getRoverX();
	    ry1 = awsobject.getRoverY();
	    tx1 = awsobject.getTargetX();
	    ty1 = awsobject.getTargetY();
      /*refresh the rover direction*/	
      rdirection1 = awsobject.getRoverDir(); 	  	  
      targetlock.lock(); 	  
      tdirection = controllerobject.gettheta(tx1, ty1, rx1, ry1);
      targetlock.unlock();
      vTaskDelay(30 / portTICK_PERIOD_MS);
    }
    
}

/**
 * @name  void taskSensors( void * parameter ) : Function/ task which runs infinitely, task is responsible for sensor aquisition
   @param void * parameter  task parameters can be passed
   @return None 
**/
void taskSensors( void * parameter )
{
    int16_t *pdata;
    for( ;; )
    {
    pdata = sensorobject.reading();             //Read sensor data
    sensorlock.lock();
    for(int i =0; i<3; i++)
    {
      SensorData[i] = *(pdata+i);               //Copy the sensor data into array 
    }
    sensorlock.unlock();     
    vTaskDelay(50 / portTICK_PERIOD_MS);
    }
 }


/**
 * @name  void taskSensors( void * parameter ) : Function/ task which runs infinitely, task is responsible for processing the 
 * given cordinates and sensor dates move rover towards the target
   @param void * parameter  task parameters can be passed
   @return None 
**/
void taskProcessor( void * parameter)
{
  /**initialize the local variables*/
  static PROCESS_STATE state = PROCESS_INIT_STATE;    //State variable
  int Statedelay = 10;                                //Variable used to dynamically change periodicity of the task
  static int statewait = 10;                          //Timer variable used to wait
  vTaskDelay(5000 / portTICK_PERIOD_MS);              //Initial delay given to press the reset button
  while(1)
  {
    /*Check the border detected if yes, hijack the state machine*/
    targetlock.lock();
    if(controllerobject.detectBorder(rdirection1, rx1, ry1)) // check if border is detected
    {
      controllerobject.writemotor(M_DIR_LAST);
      state = PROCESS_CHECK_BORDER;                       //Forcing state machine to go PROCESS_CHECK_BORDER state
    }
    targetlock.unlock();
    /*Check the Obstacle detected if yes, hijack the state machine*/
    sensorlock.lock();
    if( SensorData[1]<OBJ_DET_DIST 
    || SensorData[0]<OBJ_DET_DIST_SEC 
    || SensorData[2]<OBJ_DET_DIST_SEC )                 //Checkin the range of the obstacle
    {
      state = PROCESS_OBST_DET_STATE;                   //Forcing state machine to go PROCESS_CHECK_BORDER state
    }
    sensorlock.unlock();
    switch(state)
    {
      case PROCESS_INIT_STATE:                         //State ism used to give a start delay and re-initialize variables for future reference 
      vTaskDelay(1000 / portTICK_PERIOD_MS);
      state = PROCESS_CHECK_SLOPE_STATE;
      break;

      case PROCESS_OBST_DET_STATE:                     //if obstacle detected w.r.t sensor data take decision to move left or right.
      sensorlock.lock();
      if(SensorData[2] >= SensorData[0])
        {
          controllerobject.writemotor(M_DIR_RIGHT);
        }
        else
        {
          controllerobject.writemotor(M_DIR_LEFT);
        }
        sensorlock.unlock();
        Statedelay = 100;
        /*timer decrement logic*/
        if(statewait <= 0)                    //timeout??
        {
          state = PROCESS_OBST_DET_PROC_STAT;
          statewait = 60;                     
        }
        else
        {
          statewait-- ;                        //decrement timer
        }      
      break;

      case PROCESS_OBST_DET_PROC_STAT:        /*state is defined to move away from the obstacle*/
        controllerobject.writemotor(M_DIR_FORWARD);
        Statedelay = 60;
        /*timer decrement logic*/
        if(statewait <= 0)
        {
          state = PROCESS_CHECK_SLOPE_STATE;
          statewait = 25;
        }
        else
        {
          statewait-- ;
        }
        
      break;

      case PROCESS_CHECK_SLOPE_STATE:                                   //state is defined to process the slope information and move towards the target
      targetlock.lock();
        if(!(abs(rdirection1 - tdirection) <= ANGLE_TOL))               /* Oscillate until rover directions gets alligned with the slope*/
        {  
          /**to decide in which direction the rover should be moved*/
            if((rdirection1 - tdirection) < 0)
            {
              controllerobject.writemotor(M_DIR_LEFT);
            }
            else
            {
              controllerobject.writemotor(M_DIR_RIGHT);
            }
        }
        else /*rover direction is alligned with slope so go stright until rover reached the taget+- delta*/ 
        {
          if((TARGET_DELTA>=abs(tx1-rx1))&&(TARGET_DELTA>=abs(ty1-ry1)))  // target in range hence stop
          {
            controllerobject.writemotor(M_DIR_LAST);
          }
          else                                                            //still not reached target so go straight 
          {
            controllerobject.writemotor(M_DIR_FORWARD);
          }
        }
        targetlock.unlock();
        Statedelay = 60;
      break;
      case PROCESS_CHECK_BORDER :     //state is defined to process the data when rover is about to go outside border
      /*rover takes about 180 degree turn and move forward*/
          controllerobject.writemotor(M_DIR_RIGHT);
          delay(9000);
          controllerobject.writemotor(M_DIR_FORWARD);
          delay(3000);
        state = PROCESS_CHECK_SLOPE_STATE;
      break;
      default :
      state = PROCESS_INIT_STATE;     // default state forces the state machine to go to init state
      break ;
    }  
    vTaskDelay(Statedelay / portTICK_PERIOD_MS);    // task delay/sleep is given the amount to sleep is decided by the states
  }
}

