/**
 * ESP32 AWS Library
 * 
 * Functions to get the crawler coordinates from the Camera over AWS IoT
 * 
 * Authors: Vipul Deshpande, Jaime Burbano
 */


/*
  Copyright 2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
  Permission is hereby granted, free of charge, to any person obtaining a copy of this
  software and associated documentation files (the "Software"), to deal in the Software
  without restriction, including without limitation the rights to use, copy, modify,
  merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so.
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
  INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
  PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
#include "secrets.h"
#include <WiFiClientSecure.h>
#include <MQTTClient.h>
#include <ArduinoJson.h>
#include "WiFi.h"
#include "AWS.h"
#include <string>
#include <iostream>
#include <stdio.h>
#include <stdlib.h> 
using namespace std;
/* The MQTT topics that this device should publish/subscribe to */
#define AWS_IOT_ROVER_TOPIC   "esp32/rover"    /*Topic of the rover*/
#define AWS_IOT_TARGET_TOPIC   "esp32/target"  /*Topic of the target*/
#define AWS_IOT_PUBLISH_TOPIC  "greengrass/group06" /*Dummy topic for Publisher*/

static float rx, ry, rdirection, tx, ty; /*variables to store rover and target co-ordinates*/

WiFiClientSecure net = WiFiClientSecure();
MQTTClient client = MQTTClient(256);


bool AWS_connected =0; /*variable to store AWS connected status*/

/*Constructor for myawsclass class*/
myawsclass::myawsclass() {
}

/* Function to handle the message coming from AWS topic and extract the co-ordinates
   @param topic : subscribed topic
   @param payload : message payload
   @return null 
*/
void messageHandler(String &topic, String &payload) {
    StaticJsonDocument<200> doc;
    deserializeJson(doc, payload);
	if(topic == "esp32/rover")  /*To extract co-ordinates from the payload of rover topic*/
	{
	String rovercoordinates=doc["rover"];
	rx = (float)atoi(rovercoordinates.substring(rovercoordinates.indexOf('(')+1,(rovercoordinates.indexOf(','))).c_str());
	ry = (float)atoi(rovercoordinates.substring(rovercoordinates.indexOf(',')+2,(rovercoordinates.indexOf(')'))).c_str());
	rdirection = (float)atoi(rovercoordinates.substring(rovercoordinates.indexOf(')')+3,(rovercoordinates.indexOf(']'))).c_str());
	}
	
	
	if(topic  == "esp32/target") /*To extract co-ordinates from the payload of rover topic*/
	{
	String targetcoordinates=doc["target"];
	tx = (float)atoi(targetcoordinates.substring(targetcoordinates.indexOf('(')+1,targetcoordinates.indexOf(',')).c_str());
	ty = (float)atoi(targetcoordinates.substring(targetcoordinates.indexOf(',')+2,targetcoordinates.indexOf(')')).c_str());
	}

}
/*Getter Function to get the status of AWS connection*/
bool myawsclass::getStatus() {
return AWS_connected;
}
/*Getter Function to get the x co-ordinate of rover position*/
float myawsclass::getRoverX() {
return rx;
}

/*Getter Function to get the y co-ordinate of rover position*/
float myawsclass::getRoverY() {
return ry;
}

/*Getter Function to get the direction of rover position*/
float myawsclass::getRoverDir() {
return rdirection;
}

/*Getter Function to get the x co-ordinate of target position*/
float myawsclass::getTargetX() {
return tx;
}

/*Getter Function to get the y co-ordinate of target position*/
float myawsclass::getTargetY() {
return ty;
}
 
/*Function to run the AWS connection in infinite loop*/
void myawsclass::stayConnected() {
  client.loop();
}

/*Function to connect to AWS*/
void myawsclass::connectAWS() {

  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  Serial.println("Connecting to Wi-Fi");
  
  while (WiFi.status() != WL_CONNECTED){
    delay(500);

    Serial.print("Connecting...!");
  }

  Serial.print("CONNECTED...!\n");

  /* Configure WiFiClientSecure to use the AWS IoT device credentials */
  net.setCACert(AWS_CERT_CA);
  net.setCertificate(AWS_CERT_CRT);
  net.setPrivateKey(AWS_CERT_PRIVATE);

  /* Connect to the MQTT broker on the AWS endpoint we defined earlier */
  client.begin(AWS_IOT_ENDPOINT, 8883, net);

  /* Create a message handler */
  //delay(500);
  client.onMessage(messageHandler);

  Serial.print("Connecting to AWS IOT");

  while (!client.connect(THINGNAME)) {
    Serial.print(".");
    delay(100);
  }

  if(!client.connected())
  {
    Serial.println("AWS IoT Timeout!");
    AWS_connected = 1;

    return;
  }

  /* Subscribe to a topic */
  client.subscribe(AWS_IOT_ROVER_TOPIC);
  client.subscribe(AWS_IOT_TARGET_TOPIC);

  Serial.println("AWS IoT Connected!");
}
/*Function to publish information from the rover to AWS*/
void myawsclass::publishMessage(int16_t sensorValue) {

  StaticJsonDocument<200> doc;
  //doc["time"] = millis();
  doc["sensor"] = sensorValue;
  char jsonBuffer[512];
  serializeJson(doc, jsonBuffer); /* print to client */

  client.publish(AWS_IOT_PUBLISH_TOPIC, jsonBuffer);
}

myawsclass awsobject = myawsclass();  /* creating an object of class aws */


